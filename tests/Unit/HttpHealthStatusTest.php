<?php
namespace Tests\Unit;

// use PHPUnit\Framework\TestCase;
use Orchestra\Testbench\TestCase;
use LetFlow\LaravelApiStatus\Services\HttpHealthStatusProvider as HealthStatusProviderImpl;
use LetFlow\LaravelApiStatus\Services\HealthStatusProvider;
use LetFlow\LaravelApiStatus\ServiceProvider;
use Illuminate\Support\Facades\Http;
use Illuminate\Http\Response;

class HttpHealthStatusTest extends TestCase
{
    /**
     * Load package service provider
     * @param  \Illuminate\Foundation\Application $app
     * @return LetFlow\LaravelApiStatus\ServiceProvider
     */
    protected function getPackageProviders($app)
    {
        return [ServiceProvider::class];
    }

    /** @test */
    public function it_respects_its_contract()
    {
        $this->assertInstanceOf(HealthStatusProvider::class, new HealthStatusProviderImpl());
    }

    /** @test */
    public function test_enabled_default()
    {
        $impl = new HealthStatusProviderImpl();
        $this->assertTrue($impl->enabled());
    }

    /** @test */
    public function test_enabled_config_false()
    {
        $config = [
            'enabled' => false
        ];

        $impl = new HealthStatusProviderImpl($config);
        $this->assertFalse($impl->enabled());
    }

    /** 
     * @testWith [ "plain" ]
     *           [ "json" ] 
     * 
     */
    public function test_check_success($type)
    {
        Http::fake([
            'https://example.com/health' => match($type) {
                'plain' => Http::response('ok', Response::HTTP_OK, [ 'content-type' => 'text/plain' ]),
                'json' => Http::response(['status' => 'ok'], Response::HTTP_OK, [ 'content-type' => 'application/json' ]),
            },
            '*' => Http::response('Should not be called', 500), // replace with Http::preventStrayRequests() in Laravel 9.x
        ]);

        $config = [
            'type' => $type,
            'base_uri' => 'https://example.com',
            'check_uri' => '/health'
        ];
    
        $impl = new HealthStatusProviderImpl($config);
        $status = $impl->check();
        $this->assertArrayHasKey('status', $status);
        $this->assertEquals('ok', $status['status']);
    }

    /** 
     * @testWith    [ "status-500" ]
     */
    public function test_check_failed($reason)
    {
        Http::fake([
            'https://example.com/health' => match($reason) {
                'status-500' => Http::response('error', Response::HTTP_INTERNAL_SERVER_ERROR, [ 'content-type' => 'text/plain' ]),
                'timeout' => function ($request) {
                    sleep(3);
                    return Http::response(['status' => 'ok'], Response::HTTP_OK, [ 'content-type' => 'application/json' ]);
                }
                
            },
            '*' => Http::response('Should not be called', 500), // replace with Http::preventStrayRequests() in Laravel 9.x
        ]);

        $config = [
            'check_uri' => 'https://example.com/health',
            'timeout' => 1,
        ];

        $impl = new HealthStatusProviderImpl($config);
        $status = $impl->check();
        $this->assertArrayHasKey('status', $status);
        $this->assertEquals('failed', $status['status']);
    }
}