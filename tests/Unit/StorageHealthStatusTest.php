<?php
namespace Tests\Unit;

// use PHPUnit\Framework\TestCase;
use Orchestra\Testbench\TestCase;
use LetFlow\LaravelApiStatus\Services\StorageHealthStatusProvider as HealthStatusProviderImpl;
use LetFlow\LaravelApiStatus\Services\HealthStatusProvider;
use LetFlow\LaravelApiStatus\ServiceProvider;

class StorageHealthStatusTest extends TestCase
{
    /**
     * Load package service provider
     * @param  \Illuminate\Foundation\Application $app
     * @return LetFlow\LaravelApiStatus\ServiceProvider
     */
    protected function getPackageProviders($app)
    {
        return [ServiceProvider::class];
    }

    /** @test */
    public function it_respects_its_contract()
    {
        $this->assertInstanceOf(HealthStatusProvider::class, new HealthStatusProviderImpl());
    }

    /** @test */
    public function test_enabled_default()
    {
        $impl = new HealthStatusProviderImpl();
        $this->assertTrue($impl->enabled());
    }

    /** @test */
    public function test_enabled_config_false()
    {
        $config = [
            'enabled' => FALSE
        ];

        $impl = new HealthStatusProviderImpl($config);
        $this->assertFalse($impl->enabled());
    }
}