<?php

namespace Tests\Feature;

use Orchestra\Testbench\TestCase;
use LetFlow\LaravelApiStatus\ServiceProvider;

class StatusTest extends TestCase
{
    /**
     * Load package service provider
     * @param  \Illuminate\Foundation\Application $app
     * @return LetFlow\LaravelApiStatus\ServiceProvider
     */
    protected function getPackageProviders($app)
    {
        return [ServiceProvider::class];
    }

    /**
     * Test /status endopoint
     *
     * @return void
     */
    public function testStatus()
    {
        $response = $this->get('/status');

        $response->assertStatus(200);
    }
}
