<?php

return [

    /*
    |--------------------------------------------------------------------------
    | API STATUS
    |--------------------------------------------------------------------------
    |
    | Here you control what elements are considered in health and status pages
    |
    */

    'routes' => [
        'status' => 'status',
        'health' => 'health'
    ],

    'checks' => [
        'db' => [
            'enabled' => true
        ]
    ],

    'drivers' => [
        'db' => LetFlow\LaravelApiStatus\Services\DBHealthStatusProvider::class,
        'cache' => LetFlow\LaravelApiStatus\Services\CacheHealthStatusProvider::class,
        'disk' => LetFlow\LaravelApiStatus\Services\DiskFreeSpaceHealthStatusProvider::class,
        'storage' => LetFlow\LaravelApiStatus\Services\StorageHealthStatusProvider::class,
        'http' => LetFlow\LaravelApiStatus\Services\HttpHealthStatusProvider::class,
        'queue' => LetFlow\LaravelApiStatus\Services\QueueHealthStatusProvider::class
    ]



];
