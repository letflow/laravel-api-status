# Laravel API /status and /health routes

This package creates two routes /status and /health to expose API status and health checks.