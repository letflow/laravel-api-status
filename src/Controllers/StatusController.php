<?php

namespace LetFlow\LaravelApiStatus\Controllers;

use Illuminate\Support\Facades\Log;
use LetFlow\LaravelApiStatus\Services\HealthStatusService;

/**
 * Handle /status and /health endpoints
 */
class StatusController
{
	private static $OK = 'ok';
	private static $SKIPPED = 'skipped';
	private static $FAILED = 'failed';


    private function info()
    {
		$info = [
			'name' => config('app.name'),
			'env' => config('app.env')
		];

		foreach (config('api-status.extra', []) as $key => $value)
		{
			$info[$key] = $value;
		}

		return $info;
    }

    /**
     * Return basic server info
     */
    public function status()
    {
        $data = $this->info();
        if (defined('LARAVEL_START'))
		{
			$data['time'] = microtime(true) - LARAVEL_START; 
		}
		return response()->json($data, 200);
    }

    
    /**
     * Return health server info of each component
     */
    public function health(HealthStatusService $health)
    {
		$start = defined('LARAVEL_START') ? LARAVEL_START : microtime(true);

		$data = $this->info();		
		$checks = $health->check();
		list($checks, $status) = $this->processStatus($checks);
		
		$data['checks'] = $checks;
		$data['status'] = $status;
		$data['time'] = microtime(true) - $start;

		if ($status != self::$OK)
		{
			Log::error("Health check failed", $checks->toArray());
			return response()->json($data, 500);
		}

		Log::debug("Health check passed");
		return response()->json($data, 200);
	}

	private function processStatus($checks)
	{
		$checks = collect($checks);
		$status = $checks->where('status',self::$FAILED)->isEmpty() ? self::$OK : self::$FAILED;

		return [ $checks, $status ];		
	}
}
