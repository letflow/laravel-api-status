<?php 

namespace LetFlow\LaravelApiStatus;

use Illuminate\Support\Arr;

use Illuminate\Support\ServiceProvider as LaravelServiceProvider;
use LetFlow\LaravelApiStatus\Services\HealthStatusService;
use LetFlow\LaravelApiStatus\Services\ConfigHealthStatusService;

class ServiceProvider extends LaravelServiceProvider {

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Bootstrap the application events.
     *
     * @return void
     */
    public function boot() {

        $this->handleRoutes();
        $this->handleConfigs();

    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register() {
        $this->app->bind(HealthStatusService::class, function ($app) {
            return new ConfigHealthStatusService();
        });
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides() {

        return [];
    }

    private function handleRoutes() {
        $this->loadRoutesFrom(__DIR__.'/routes.php');
    }

    private function handleConfigs() {

        $configPath = __DIR__ . '/../config/api-status.php';

        $this->publishes([$configPath => config_path('api-status.php')]);

        $this->mergeConfigFrom($configPath, 'api-status');
    }

    // merge the config in multi-dimensional arrays
    // https://medium.com/@koenhoeijmakers/properly-merging-configs-in-laravel-packages-a4209701746d

    /**
     * Merge the given configuration with the existing configuration.
     *
     * @param  string  $path
     * @param  string  $key
     * @return void
     */
    protected function mergeConfigFrom($path, $key)
    {
        $config = $this->app['config']->get($key, []);
        $this->app['config']->set($key, $this->mergeConfig(require $path, $config));
    }

    /**
     * Merges the configs together and takes multi-dimensional arrays into account.
     *
     * @param  array  $original
     * @param  array  $merging
     * @return array
     */
    protected function mergeConfig(array $original, array $merging)
    {
        $array = array_merge($original, $merging);
        foreach ($original as $key => $value) {
            if (! is_array($value)) {
                continue;
            }
            if (! Arr::exists($merging, $key)) {
                continue;
            }
            if (is_numeric($key)) {
                continue;
            }
            $array[$key] = $this->mergeConfig($value, $merging[$key]);
        }
        return $array;
    }

}
