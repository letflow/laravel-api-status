<?php

namespace LetFlow\LaravelApiStatus\Services;

use Cache;
use Log;

class DiskFreeSpaceHealthStatusProvider implements HealthStatusProvider
{
    protected $path;
    protected $enabled;
    protected $minFree;

    /**
     * Check DB connection health
     * 
     * $connection  The name of DB connection. Will use detault connection if not provided
     */
    function __construct($config=null)
    {
        $this->enabled = $config['enabled'] ?? true;
        $this->path = $config['path'] ?? '/';
        $this->minFree = $config['min'] ?? '100m';
    }

    public function enabled()
    {
        return $this->enabled === true;
    }

    public function check()
    {
        $free = disk_free_space($this->path);
        $min_disk_space = $this->getDiskSpace($this->path, $this->minFree);

        $status = ($free > $min_disk_space) ? "ok" : "failed";

		return [ 
			"status" => $status,
			"free" => $this->formatBytes($free)
		 ];
    }

    private function getDiskSpace($path, $value)
    {
        // check if $value is a percent
        $perc_idx = strrpos($value, '%');
        if ($perc_idx  === FALSE)
        {
            return $this->human2byte($value);
        }
        
        $perc = substr($value, 0, $perc_idx);
        $total = disk_total_space($path);
        $value = $total * $perc / 100;
        return $value;
    }

    function formatBytes($size, $precision = 2)
	{
		$base = log($size, 1024);
		$suffixes = array('', 'K', 'M', 'G', 'T');   
	
		return round(pow(1024, $base - floor($base)), $precision) . ' ' . $suffixes[floor($base)] . 'B';
    }
    
    /**
     * Converts a human readable file size value to a number of bytes that it
     * represents. Supports the following modifiers: K, M, G and T.
     * Invalid input is returned unchanged.
     *
     * Example:
     * <code>
     * $config->human2byte(10);          // 10
     * $config->human2byte('10b');       // 10
     * $config->human2byte('10k');       // 10240
     * $config->human2byte('10K');       // 10240
     * $config->human2byte('10kb');      // 10240
     * $config->human2byte('10Kb');      // 10240
     * // and even
     * $config->human2byte('   10 KB '); // 10240
     * </code>
     *
     * @param number|string $value
     * @return number
     */
    function human2byte($value) {
        return preg_replace_callback('/^\s*(\d+)\s*(?:([kmgt]?)b?)?\s*$/i', function ($m) {
        switch (strtolower($m[2])) {
            case 't': $m[1] *= 1024;
            case 'g': $m[1] *= 1024;
            case 'm': $m[1] *= 1024;
            case 'k': $m[1] *= 1024;
        }
        return $m[1];
        }, $value);
    }
}