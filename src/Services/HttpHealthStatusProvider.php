<?php

namespace LetFlow\LaravelApiStatus\Services;

use Illuminate\Http\Client\ConnectionException;
use Illuminate\Support\Facades\Http;
use LetFlow\LaravelApiStatus\Services\HealthStatusProvider;

class HttpHealthStatusProvider implements HealthStatusProvider
{
    protected $enabled;
    protected $config;

    function __construct($config=null)
    {
        $this->config = $config;
        $this->enabled = $config['enabled'] ?? true;
    }

    public function enabled()
    {
        return $this->enabled === true;
    }

    public function check()
    {
        $baseUri = $this->config['base_uri'] ?? '';
        $checkUri = $this->config['check_uri'] ?? '/status';
        $type = $this->config['type'] ?? 'plain';
        $timeout = $this->config['timeout'] ?? 30;

        try {
            $request = Http::baseUrl($baseUri)
                ->timeout($timeout);

            if (method_exists($request, 'connectTimeout')) {
                $request->connectTimeout($timeout);
            }

            $response = $request->get($checkUri);
        }
        catch (ConnectionException $e) {
            return [
                "status" => 'failed',
                "response" => 'timeout',
            ];
        }

        // if it fails, do not parse response
        if ($response->failed()) {
            $type = 'plain';
        }

        $body = $type === 'json' ? json_decode($response->body(), true) : $response->body();
        
        $status = [
            "status" => $response->successful() ? 'ok' : 'failed',
            "response" => $body,
        ];

        if ($response->failed())
        {
            $status["code"] = $response->getStatusCode();
        }
        
        return $status;
	}
}