<?php

namespace LetFlow\LaravelApiStatus\Services;

use Illuminate\Support\Facades\Log;

class ConfigHealthStatusService implements HealthStatusService
{
    public function check()
    {
        $items = config('api-status.checks');
        $results = [];
        foreach($items as $item => $config)
        {
            $results[$item] = $this->process($item, $config);
        }
        return $results;
    }

    private function process($item, $config)
    {
        $provider = $this->makeProvider($item, $config);

        $start = microtime(true);
        try
        {
            $result = $provider->enabled() ? $provider->check() : null;

        }
        catch(\Exception $e)
        {
            Log::error("Error running check on ".$item, [ 'exception' => $e ]);
            $result = $e->getMessage();
        }
        $time = microtime(true) - $start;

        if ($result === null || $result === true)
        {
            $result = [ 'status' => 'ok' ];
        }
        elseif ($result === false) {
            $result = [ 'status' => 'failed' ];
        }
        elseif (is_string($result)) {
            $result = [ 'status' => 'failed', 'message' => $result ];
        }
        elseif (! is_array($result)) {
            $result = [ 'status' => 'failed', 'message' => "Unsupported health check result ".get_class($result) ];
        }

        $result['time'] = round($time, 2) . ' microseconds';
        return $result;

    }

    private function makeProvider($item, $config)
    {
        $driver = $config['driver'] ?? null;
        if (! $driver)
        {
            throw new \InvalidArgumentException("Cannot make health provider. Driver is required. Key=".$item);
        }

        // map driver name to class, if exists
        $driver = config('api-status.drivers.'.$driver, $driver);
        return new $driver($config);
    }
}