<?php

namespace LetFlow\LaravelApiStatus\Services;

use Illuminate\Support\Facades\Queue;
use LetFlow\LaravelApiStatus\Services\HealthStatusProvider;

class QueueHealthStatusProvider implements HealthStatusProvider
{
    protected $enabled;
    protected $queue;

    function __construct($config=null)
    {
        $this->enabled = $config['enabled'] ?? true;
        $this->queue = $config['queue'] ?? null;
    }

    public function enabled()
    {
        return $this->enabled === true;
    }

    public function check()
    {
        // fail on error
        $size = Queue::connection($this->queue)->size();
		return [
            "status" => "ok",
            "size" => $size
        ];;
    }
}