<?php

namespace LetFlow\LaravelApiStatus\Services;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Str;

class CacheHealthStatusProvider implements HealthStatusProvider
{
    protected $store;
    protected $enabled;

    function __construct($config=null)
    {
        $this->enabled = $config['enabled'] ?? true;
        $this->store = $config['store'] ?? null;
    }

    public function enabled()
    {
        return $this->enabled === true;
    }

    public function check()
    {
        // Test cache connection
		$key = Str::random(10);
		$val = Str::random(40);
        $store = Cache::store($this->store);
        $store->put($key, $val, 1);
        $res = $store->get($key);

        if ($val != $res)
        {
            return "Invalid cache value. Key=".$key." Val=".$val." but got:".$res;
        }

		return true;
    }
}