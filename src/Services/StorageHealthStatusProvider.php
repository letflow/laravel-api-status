<?php

namespace LetFlow\LaravelApiStatus\Services;

use Illuminate\Support\Facades\Storage;

class StorageHealthStatusProvider implements HealthStatusProvider
{
    protected $disk;
    protected $enabled;

    function __construct($config=null)
    {
        $this->enabled = $config['enabled'] ?? true;
        $this->disk = $config['disk'] ?? null;
    }

    public function enabled()
    {
        return $this->enabled === true;
    }

    public function check()
    {
        $storage = Storage::disk($this->disk);

        $path = $storage->path("");
		$readable = is_readable($path);
		$writable = is_writable($path);

        $directories = $storage->directories();
        return [
            "status" => "ok",
            "readable" => $readable,
            "writable" => $writable,
            "directories" => $directories
        ];
    }
}