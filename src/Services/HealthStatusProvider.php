<?php

namespace LetFlow\LaravelApiStatus\Services;

interface HealthStatusProvider
{
    public function enabled();
    public function check();
}