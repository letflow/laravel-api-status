<?php

namespace LetFlow\LaravelApiStatus\Services;

use Illuminate\Support\Facades\DB;

class DBHealthStatusProvider implements HealthStatusProvider
{
    protected $connection;
    protected $enabled;

    function __construct($config=null)
    {
        $this->enabled = $config['enabled'] ?? true;
        $this->connection = $config['connection'] ?? null;
    }

    public function enabled()
    {
        return $this->enabled === true;
    }

    public function check()
    {
        // Test database connection
        DB::connection($this->connection)->getPdo();
        return true;
    }
}