<?php

namespace LetFlow\LaravelApiStatus\Services;

interface HealthStatusService
{
    public function check();
}