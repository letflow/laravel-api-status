<?php

use Illuminate\Http\Request;

Route::get(config('api-status.routes.status','/status'), '\LetFlow\LaravelApiStatus\Controllers\StatusController@status');
Route::get(config('api-status.routes.health','/health'), '\LetFlow\LaravelApiStatus\Controllers\StatusController@health');